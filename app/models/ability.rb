# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # Guest user (not logged in)

    if user.admin?
      can :manage, :all # Admins can manage all resources
    elsif user.user?
      can %i[read create update], :all
    end
  end
end
