# frozen_string_literal: true

class ArticlesController < ApplicationController
  include Pagy::Backend
  before_action :set_article, only: %i[show update destroy]
  load_and_authorize_resource

  def index
    @pagy, @articles = pagy(Article.all, items: 3)
    response_data = {
      pagination: { current_page: @pagy.page,
                    total_pages: @pagy.pages,
                    next_page: @pagy.next,
                    prev_page: @pagy.prev },
      data: ArticleSerializer.new(@articles).serializable_hash[:data].map { |item| item[:attributes] }
    }
    render json: response_data
  end

  def show
    render json: ArticleSerializer.new(@article).serializable_hash[:data][:attributes]
  end

  def create
    @article = current_user.articles.create(article_params)
    render json: ArticleSerializer.new(@article).serializable_hash[:data][:attributes]
  end

  def update
    @article.update(article_params)
    render json: ArticleSerializer.new(@article).serializable_hash[:data][:attributes]
  end

  def destroy
    @article.destroy
    @articles = Article.all
    render json: ArticleSerializer.new(@article).serializable_hash[:data][:attributes]
  end

  private

  def set_article
    @article = Article.find(params[:id])
  end

  def article_params
    params.require(:article).permit(:title, :body)
  end
end
