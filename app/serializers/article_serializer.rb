# frozen_string_literal: true

class ArticleSerializer
  include JSONAPI::Serializer
  attributes :id, :title, :body, :user_id
end
