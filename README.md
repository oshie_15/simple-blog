<!-- GETTING STARTED -->

## 💻 Getting Started <a name="getting-started"></a>

To get a local copy up and running, follow these steps.

### Prerequisites

In order to run this project you need:

```sh
 gem install rails
```
```sh
 Docker
```

### Setup

Clone this repository to your desired folder:

```sh
  git clone git@gitlab.com:oshie_15/simple-blog.git

```

### Usage

To run the project, execute the following command:


```sh
Remove 'credentials.yml.enc'
```
```sh 
#VSCode 
EDITOR='code --wait' rails credentials:edit
# This command will create secret base key
```

```sh
Create your own .env file as shown .env.example
```

```sh
  docker-compose build
```
```sh
  docker-compose up -d
```
### Run


```sh 
  docker-compose exec app rails db:create
``` 

```sh 
  docker-compose exec app rails db:migrate
``` 
```sh 
  docker-compose exec app rails db:seed
  #it takes little bit time
``` 
### Run Rspec test
```sh 
  docker-compose exec app rails db:migrate RAILS_ENV=test
``` 
```sh 
  docker-compose exec app rspec