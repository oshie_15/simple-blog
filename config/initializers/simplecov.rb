# frozen_string_literal: true

# config/initializers/simplecov.rb

require 'simplecov'

SimpleCov.start 'rails' do
  # Exclude directories and files from coverage report
  add_filter '/spec/' # Exclude spec directory
  add_filter '/config/' # Exclude config directory

  # Set a minimum coverage threshold (optional)
  minimum_coverage 95 # Adjust as needed
end
