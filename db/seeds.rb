# frozen_string_literal: true

# 1000.times do
#   User.create(
#     email: Faker::Internet.email,
#     password: Faker::Internet.password
#   )
# end
user = User.create(email: 'email@mail.com', password: 'hardpassword', role: 'admin')

1000.times do
  Article.create(
    title: Faker::Books::Lovecraft.location,
    body: Faker::Books::Lovecraft.paragraph,
    user:
  )
end
