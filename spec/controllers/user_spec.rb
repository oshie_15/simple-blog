# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Users::RegistrationsController, type: :controller do
  before do
    @request.env['devise.mapping'] = Devise.mappings[:user]
  end

  describe 'POST #create' do
    context 'when user is successfully created' do
      it 'returns a success response with user JSON' do
        valid_user_params = { email: 'user@example.com', password: 'password' }
        post :create, params: { user: valid_user_params }
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to start_with('application/json')
      end
    end

    context 'when user creation fails' do
      it 'returns an unprocessable entity response with errors' do
        invalid_user_params = { email: 'invalid_email', password: 'password' }
        post :create, params: { user: invalid_user_params }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to start_with('application/json')
      end
    end
  end
end
