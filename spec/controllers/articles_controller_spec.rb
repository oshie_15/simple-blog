# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ArticlesController, type: :controller do
  let(:user) { FactoryBot.create(:user) }

  before do
    sign_in user
  end

  describe 'GET #index' do
    it 'returns a success response' do
      get :index
      expect(response).to be_successful
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      article = FactoryBot.create(:article, user: user)
      get :show, params: { id: article.to_param }
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Article' do
        valid_attributes = { title: 'Sample Article', body: 'Sample body content' }
        expect do
          post :create, params: { article: valid_attributes }
        end.to change(Article, :count).by(1)
      end

      it 'returns a success response' do
        valid_attributes = { title: 'Sample Article', body: 'Sample body content' }
        post :create, params: { article: valid_attributes }
        expect(response).to be_successful
      end
    end

    context 'with invalid params' do
      it 'returns an unprocessable entity response' do
        invalid_attributes = { title: nil, body: 'Sample body content' }
        post :create, params: { article: invalid_attributes }
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'PATCH #update' do
    context 'with valid params' do
      it 'updates the requested article' do
        article = FactoryBot.create(:article, user: user)
        new_attributes = { title: 'Updated Title' }
        patch :update, params: { id: article.to_param, article: new_attributes }
        article.reload
        expect(article.title).to eq('Updated Title')
      end

      it 'returns a success response' do
        article = FactoryBot.create(:article, user: user)
        new_attributes = { title: 'Updated Title' }
        patch :update, params: { id: article.to_param, article: new_attributes }
        expect(response).to be_successful
      end
    end

    context 'with invalid params' do
      it 'returns an unprocessable entity response' do
        article = FactoryBot.create(:article, user: user)
        invalid_attributes = { title: nil }
        patch :update, params: { id: article.to_param, article: invalid_attributes }
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested article' do
      article = FactoryBot.create(:article, user: user)
      expect do
        delete :destroy, params: { id: article.to_param }
      end.to change(Article, :count).by(-1)
    end

    it 'returns a success response' do
      article = FactoryBot.create(:article, user: user)
      delete :destroy, params: { id: article.to_param }
      expect(response).to be_successful
    end
  end
end
