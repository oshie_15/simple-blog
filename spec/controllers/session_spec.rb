# frozen_string_literal: true

# spec/controllers/session_spec.rb

require 'rails_helper'

RSpec.describe Users::SessionsController, type: :controller do
  # Define a sample user for testing
  let(:user) { FactoryBot.create(:user) }

  before do
    # Sign in the user using Devise test helpers (assuming you are using Devise)
    sign_in user
  end

  describe 'POST #create', controller: 'users/sessions' do
    # ...
  end

  describe 'DELETE #destroy', controller: 'users/sessions' do
    it 'signs out the user and returns a JSON response with status 200' do
      @request.env['devise.mapping'] = Devise.mappings[:user] # Set Devise mapping

      # Stub JWT decoding for the request header
      allow(JWT).to receive(:decode).and_return([{ 'sub' => user.id }])

      # Set the Authorization header with a valid JWT token
      valid_jwt_token = JWT.encode({ 'sub' => user.id }, Rails.application.credentials.fetch(:secret_key_base),
                                   'HS256')
      request.headers['Authorization'] = "Bearer #{valid_jwt_token}"

      delete :destroy, format: :json

      expect(response).to have_http_status(:ok)
      expect(JSON.parse(response.body)).to eq(
        'status' => 200,
        'message' => 'Logged out successfully.'
      )
    end
  end
end
