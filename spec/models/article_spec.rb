# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Article, type: :model do
  describe 'validations' do
    it 'validates presence of title' do
      article = Article.new(body: 'Sample body')
      expect(article).to_not be_valid
      expect(article.errors[:title]).to include("can't be blank")
    end

    it 'validates presence of body' do
      article = Article.new(title: 'Sample title')
      expect(article).to_not be_valid
      expect(article.errors[:body]).to include("can't be blank")
    end
  end

  describe 'associations' do
    it 'belongs to a user' do
      user = User.create(email: 'user@example.com', password: 'password')
      article = user.articles.build(title: 'Sample title', body: 'Sample body')

      expect(article.user).to eq(user)
    end
  end

  describe 'custom methods' do
    it 'can be created' do
      user = User.create(email: 'user@example.com', password: 'password')
      article = user.articles.build(title: 'Sample title', body: 'Sample body')

      expect(article.save).to be true
      expect(Article.count).to eq(1)
    end

    it 'can be updated' do
      user = User.create(email: 'user@example.com', password: 'password')
      article = user.articles.create(title: 'Sample title', body: 'Sample body')

      updated_attributes = { title: 'Updated title', body: 'Updated body' }
      article.update(updated_attributes)

      expect(article.reload.title).to eq(updated_attributes[:title])
      expect(article.reload.body).to eq(updated_attributes[:body])
    end

    it 'can be destroyed' do
      user = User.create(email: 'user@example.com', password: 'password')
      article = user.articles.create(title: 'Sample title', body: 'Sample body')

      expect { article.destroy }.to change(Article, :count).by(-1)
    end
  end
end
