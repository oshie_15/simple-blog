# frozen_string_literal: true

# spec/models/user_spec.rb
require 'rails_helper'

RSpec.describe User, type: :model do
  # Define a user using FactoryBot's :user factory.
  let(:user) { FactoryBot.create(:user) }

  describe 'Associations' do
    it 'has many articles' do
      # Create a user using FactoryBot's :user factory
      user = FactoryBot.create(:user)

      # Create one or more articles associated with the user
      article1 = FactoryBot.create(:article, user: user)
      article2 = FactoryBot.create(:article, user: user)

      # Use the user's association to check if the articles are present
      expect(user.articles).to include(article1, article2)
    end

    # You can add more association tests as needed
  end

  # Test Devise-related functionality (authentication, etc.).
  describe 'Devise' do
    it 'is valid with valid attributes' do
      expect(user).to be_valid
    end

    it 'is not valid without an email' do
      user.email = nil
      expect(user).not_to be_valid
    end
    it 'is not valid without password' do
      user.password = nil
      expect(user).not_to be_valid
    end
    it 'too short password' do
      user.password = 'abs123'
      expect(user).not_to be_valid
    end
    it 'too short password' do
      user.password = 'abs123456'
      expect(user).to be_valid
    end
  end

  describe 'Custom Methods' do
    it 'returns true for admin? when role is admin' do
      user.role = 'admin'
      expect(user.admin?).to be true
    end

    it 'returns false for admin? when role is user' do
      user.role = 'user'
      expect(user.admin?).to be false
    end

    it 'returns true for user? when role is user' do
      user.role = 'user'
      expect(user.user?).to be true
    end

    it 'returns false for user? when role is admin' do
      user.role = 'admin'
      expect(user.user?).to be false
    end

    # Add more custom method tests as needed.
  end
end
