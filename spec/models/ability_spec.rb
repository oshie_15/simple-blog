# frozen_string_literal: true

require 'rails_helper'

require 'cancancan'

RSpec.describe Ability, type: :model do
  describe 'Abilities' do
    context 'when the user is an admin' do
      let(:user) { create(:user, role: 'admin') }
      let(:ability) { Ability.new(user) }

      it 'can manage all resources' do
        expect(ability.can?(:manage, :all)).to be_truthy
      end
    end
    context 'when the user is an user' do
      let(:user) { create(:user) }
      let(:ability) { Ability.new(user) }

      it 'can read all resources' do
        expect(ability.can?(:read, :all)).to be_truthy
      end

      it 'can create resources' do
        expect(ability.can?(:create, :all)).to be_truthy
      end
      it 'can update resources' do
        expect(ability.can?(:update, :all)).to be_truthy
      end

      it 'cannot delete resources' do
        expect(ability.can?(:destroy, :all)).to be_falsy
      end
    end
  end
end
