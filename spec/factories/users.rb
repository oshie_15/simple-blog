# frozen_string_literal: true

# spec/factories/users.rb
FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    password { 'password123' } # Adjust this as needed for your password requirements
    role { 'user' } # Default role
  end
end
