# frozen_string_literal: true

FactoryBot.define do
  factory :article do
    title { 'Sample Article' }
    body { 'Sample body content' }
    user # This sets up the association with a user automatically
  end
end
